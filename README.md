# IndigoExercise

Program written in **Python 3** that computes some basic statistics on a collection of small positive integers.

## Requirements

Just needs **python 3** to run the application

## Getting started

To use the DataCapture it is needed to instance the integer implementation and add some numbers
```python
capture = IntegerDataCapture()
capture.add(3)
capture.add(9)
capture.add(3)
capture.add(4)
capture.add(6)
```

To compute the data
```python
capture.build_stats()
```

Less method
```python
capture.less(4)
# This returns [3, 3]
```

Greater method
```python
capture.greater(4)
# This returns [6, 9]
```
 
 Between method
 ```python
capture.between(3, 6)
# This returns [3, 3, 4, 6]
```
 
## Run tests
In the repository root run next command
```text
python -m unittest discover test
```

***