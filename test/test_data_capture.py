import unittest
from app.integer_data_capture import IntegerDataCapture


class TestDataCapture(unittest.TestCase):

    def setUp(self):
        """
        Set up IntegerDataCapture
        """
        self.data_capture = IntegerDataCapture()

    def test_empty_list(self):
        """
        Test an empty list
        """
        self.data_capture.build_stats()
        self.assertEqual(self.data_capture.data, [])

    def test_less_all(self):
        """
        Test less method including all numbers
        """
        self.data_capture.add(3)
        self.data_capture.add(5)
        self.data_capture.add(7)
        self.data_capture.build_stats()
        expected = [3, 5, 7]
        self.assertEqual(self.data_capture.less(8), expected)

    def test_less_some_numbers(self):
        """
        Test less method including some numbers
        """
        self.data_capture.add(3)
        self.data_capture.add(5)
        self.data_capture.add(7)
        self.data_capture.build_stats()
        expected = [3, 5]
        self.assertEqual(self.data_capture.less(7), expected)

    def test_less_one_number(self):
        """
        Test less method including one number
        """
        self.data_capture.add(3)
        self.data_capture.add(5)
        self.data_capture.add(7)
        self.data_capture.build_stats()
        expected = [3]
        self.assertEqual(self.data_capture.less(4), expected)

    def test_less_duplicated_numbers(self):
        """
        Test less method with duplicated numbers
        """
        self.data_capture.add(3)
        self.data_capture.add(5)
        self.data_capture.add(7)
        self.data_capture.add(3)
        self.data_capture.build_stats()
        expected = [3, 3]
        self.assertEqual(self.data_capture.less(4), expected)

    def test_less_empty(self):
        """
        Test less method with empty result
        """
        self.data_capture.add(3)
        self.data_capture.add(5)
        self.data_capture.add(7)
        self.data_capture.build_stats()
        expected = []
        self.assertEqual(self.data_capture.less(3), expected)

    def test_greater_all_numbers(self):
        """
        Test greater method including all numbers
        """
        self.data_capture.add(3)
        self.data_capture.add(5)
        self.data_capture.add(7)
        self.data_capture.build_stats()
        expected = [3, 5, 7]
        self.assertEqual(self.data_capture.greater(1), expected)

    def test_greater_some_numbers(self):
        """
        Test greater method including some numbers
        """
        self.data_capture.add(3)
        self.data_capture.add(5)
        self.data_capture.add(7)
        self.data_capture.build_stats()
        expected = [5, 7]
        self.assertEqual(self.data_capture.greater(3), expected)

    def test_greater_empty(self):
        """
        Test greater method with an empty result
        """
        self.data_capture.add(3)
        self.data_capture.add(5)
        self.data_capture.add(7)
        self.data_capture.build_stats()
        expected = []
        self.assertEqual(self.data_capture.greater(7), expected)

    def test_greater_bigger_number(self):
        """
        Test greater method with number not in data
        """
        self.data_capture.add(3)
        self.data_capture.add(5)
        self.data_capture.add(7)
        self.data_capture.build_stats()
        expected = []
        self.assertEqual(self.data_capture.greater(25), expected)

    def test_greater_duplicated_numbers(self):
        """
        Test greater method with duplicated numbers
        """
        self.data_capture.add(3)
        self.data_capture.add(5)
        self.data_capture.add(7)
        self.data_capture.add(5)
        self.data_capture.build_stats()
        expected = [5, 5, 7]
        self.assertEqual(self.data_capture.greater(3), expected)

    def test_between_not_existing_range(self):
        """
        Test between method with an not existing range
        """
        self.data_capture.add(3)
        self.data_capture.add(5)
        self.data_capture.add(7)
        self.data_capture.build_stats()
        expected = []
        self.assertEqual(self.data_capture.between(8, 15), expected)

    def test_between_existing_range_for_all_numbers(self):
        """
        Test between method with a range that includes all numbers
        """
        self.data_capture.add(3)
        self.data_capture.add(11)
        self.data_capture.add(5)
        self.data_capture.add(7)
        self.data_capture.add(1)
        self.data_capture.build_stats()
        expected = [1, 3, 5, 7, 11]
        self.assertEqual(self.data_capture.between(1, 11), expected)

    def test_between_existing_range(self):
        """
        Test between method with a range that exists
        """
        self.data_capture.add(3)
        self.data_capture.add(11)
        self.data_capture.add(5)
        self.data_capture.add(7)
        self.data_capture.add(1)
        self.data_capture.build_stats()
        expected = [3, 5, 7]
        self.assertEqual(self.data_capture.between(3, 7), expected)

    def test_between_duplicated_numbers(self):
        """
        Test between method with duplicated numbers
        """
        self.data_capture.add(3)
        self.data_capture.add(11)
        self.data_capture.add(5)
        self.data_capture.add(7)
        self.data_capture.add(1)
        self.data_capture.add(7)
        self.data_capture.build_stats()
        expected = [3, 5, 7, 7]
        self.assertEqual(self.data_capture.between(3, 7), expected)

    def test_less_negative_numbers(self):
        """
        Test less method with negative numbers
        """
        self.data_capture.add(-2)
        self.data_capture.add(-3)
        self.data_capture.build_stats()
        expected = []
        self.assertEqual(self.data_capture.less(1), expected)

    def test_greater_negative_numbers(self):
        """
        Test greater method with negative numbers
        """
        self.data_capture.add(-2)
        self.data_capture.add(-3)
        self.data_capture.build_stats()
        expected = []
        self.assertEqual(self.data_capture.greater(1), expected)

    def test_between_negative_numbers(self):
        """
        Test between method with negative numbers
        """
        self.data_capture.add(-2)
        self.data_capture.add(-3)
        self.data_capture.build_stats()
        expected = []
        self.assertEqual(self.data_capture.between(1, 8), expected)
