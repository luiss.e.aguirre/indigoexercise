"""
Class that implements DataCapture for positive integers
"""
from app.data_capture import DataCapture


class IntegerDataCapture(DataCapture):
    """
    Class to handle positive integers
    """

    def __init__(self, max_number=1000):
        self.data = []
        self.index_map = {}
        self.MAX_NUMBER = max_number

    @staticmethod
    def _fill_map(numbers_map):
        pass

    def build_stats(self):
        """
        Method to process the data and have the stats available
        """
        # Empty mapping dict
        self.index_map = {}

        # Sort array to start mapping
        self.data = sorted(self.data)

        # Collect all possible values
        array_index = 0
        start = 0
        end = 0
        previous_number = 0
        for number in range(1, self.MAX_NUMBER):
            # For duplicated values
            if array_index < len(self.data) and self.data[array_index] == previous_number:
                start += 1
                self.index_map[self.data[array_index]] = (start, end-1)
                end += 1
                array_index += 1
            # When number is found
            if array_index < len(self.data) and number == self.data[array_index]:
                previous_number = number
                start += 1
                self.index_map[number] = (start, end)
                end += 1
                array_index += 1
            # When number is not in data
            else:
                self.index_map[number] = (start, end)

    def add(self, number):
        """
        Method to add an number
        :param number: Number to be added
        :return True if addition was successfully otherwise returns False
        """
        if number > 1000 or number < 0:
            return False
        self.data.append(number)
        return True

    def less(self, number):
        """
        Method to retrieve all numbers less than given 'number'
        :param number: Number to compare
        :return: List with numbers less than 'number'
        """
        return self.data[:self.index_map[number][1]]

    def greater(self, number):
        """
        Method to retrieve all numbers greater than given 'number'
        :param number: Number to compare
        :return: List with numbers greater than 'number'
        """
        return self.data[self.index_map[number][0]:]

    def between(self, start, end):
        """
        Method to retrieve all numbers between 'start' and 'end' including them
        :param start: Start number to compare
        :param end: End number to compare
        :return: List with numbers between 'start' and 'end' including them
        """
        return self.data[self.index_map[start][1]:self.index_map[end][0]]
