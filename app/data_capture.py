"""
Interface to define DataCapture methods
"""
import abc


class DataCapture:

    def __init__(self, data):
        self.data = data

    @abc.abstractmethod
    def build_stats(self):
        """
        Method to process the data and have the stats available
        """
        raise NotImplementedError("Build stats not implemented")

    @abc.abstractmethod
    def add(self, element):
        """
        Method to add an element
        :param element: Element to be added
        :return True if addition was successfully otherwise returns False
        """
        raise NotImplementedError("Add not implemented")

    @abc.abstractmethod
    def less(self, element):
        """
        Method to search elements less than the given element
        :param element: Element to compare
        :return: a List of elements than are lower than 'element'
        """
        raise NotImplementedError("Less not implemented")

    @abc.abstractmethod
    def greater(self, element):
        """
        Method to search elements greater than the given element
        :param element: Element to compare
        :return: a List of elements than are greater than  'element'
        """
        raise NotImplementedError("Greater not implemented")

    @abc.abstractmethod
    def between(self, start, end):
        """
        Method to search between a range of elements [start, end]
        :param start: Start element to compare
        :param end: End element to compare
        :return: a List of elements that are in the [start, end] range
        """
        raise NotImplementedError("Between not implemented")
